<?php

include 'config.php';
include 'JSSDK.php';
$jssdk = new JSSDK($appid, $appsecret);
$url = $_GET['url'];
$signPackage = $jssdk->getSignPackage($url);
$config =  array(
    'debug' => true,
    'appId' => $signPackage['appId'],
    'timestamp' => $signPackage['timestamp'],
    'nonceStr' => $signPackage['nonceStr'],
    'signature' => $signPackage['signature'],
    'jsApiList' => array(
        'checkJsApi',
        'updateTimelineShareData',
        'hideOptionMenu',
        'updateAppMessageShareData',
        'hideMenuItems',
        'showMenuItems',
        'onMenuShareAppMessage',
        'onMenuShareTimeline',
    )
);
echo json_encode(['code' => 0, 'data' => $config, 'msg' => 'ok']);
exit;
