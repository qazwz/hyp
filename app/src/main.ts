//vue3框架提供的方法 createApp方法，可以用来创建应用实例
import { createApp } from "vue"
//引入清除默认样式
import '@/assets/style/reset.scss'
//引入跟组件app
import App from "./App.vue"

import router from "./router"
import store from "./store"
import gloalComponent from './components'



//利用createAPP方法创建应用实例，且将应用实例挂在到挂载点
const app = createApp(App)

app.use(router)
app.use(store)
app.use(gloalComponent) 

//挂在
app.mount("#app")
