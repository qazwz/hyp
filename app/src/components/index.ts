//引入项目的全部组件
import { App } from 'vue'
import SvgIcon from './SvgIcon/index.vue'
import Pagination from './Pagination/index.vue'

//全局对象
const allGloablComponent = { SvgIcon, Pagination }

export default {
    install(app: App) {
        Object.entries(allGloablComponent).forEach(([key,value]) => {
            app.component(key, value)
        })
    },
}
