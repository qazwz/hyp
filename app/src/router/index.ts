import { createRouter, createWebHistory } from 'vue-router'
import { Route } from './routers'
export default createRouter({
  //路由模式
  history: createWebHistory(),
  routes: Route,
  //滚动行为
  scrollBehavior() {
    //console.log(to, from, savedPosition)
    return {
      left: 0,
      top: 0,
    }
  },
})
