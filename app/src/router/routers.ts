export const Route = [
  {
    path: '/home',
    component: () => import('@/views/home/index.vue'),
    name: 'home',
  },
  {
    path: '/zhibo',
    component: () => import('@/views/home/zhibo.vue'),
    name: 'zhibo',
  },
  {
    path: '/jianjie',
    component: () => import('@/views/home/jianjie.vue'),
    name: 'jianjie',
  },
  {
    path: '/richeng',
    component: () => import('@/views/home/richeng.vue'),
    name: 'richeng',
  },
  {
    path: '/shipin',
    component: () => import('@/views/home/shipin.vue'),
    name: 'shipin',
  },
  {
    path: '/helloworld',
    component: () => import('@/views/helloworld/index.vue'),
    name: 'helloworld',
  },
  {
    path: '/',
    component: () => import('@/views/home/index.vue'),
    name: 'index',
  },
  {
    path: '/404',
    component: () => import('@/views/404/index.vue'),
    name: '404',
  },
  {
    path: '/:pathMatch(.*)*',
    // redirect: '/404',//重定向404
    component: () => import('@/views/404/index.vue'),
    name: 'any',
  },
]
