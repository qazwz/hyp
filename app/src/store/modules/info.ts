import { defineStore } from 'pinia'

//第一个参数：小仓库名字，第二个参数：小仓库的配置对象
let useInfoStore = defineStore('info', {
    //小仓库存储数据地方
    state: () => {
        return {
            count: 99,
            arr: [1, 2, 3, 4, 5, 6, 7, 8, 9],
        }
    },
    //异步|逻辑的地方
    actions: {
        updateNumber() {
            this.count = 100
        },
    },
    getters: {
        total() {
            this.arr.reduce((prev: number, next: number) => {
                return prev + next
            }, 0)
        },
    },
})

export default useInfoStore
