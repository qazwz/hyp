import { defineStore } from 'pinia'
import { ref } from 'vue'

let useTodoStore = defineStore('todo', () => {
    let todos = ref(111)
    let updateTodos1 = () => {
        todos.value = 333
    }
    //务必要返回一个对象，属性与方法可以提供给组件使用
    return {
        todos,
        updateTodos() {
            todos.value = 222
        },
        updateTodos1,
    }
})

export default useTodoStore
