//axios的二次封装
import axios from 'axios'
import { showMessage } from './status'
import QS from 'qs'

// let request = axios.create({
//   baseURL: import.meta.env.VITE_APP_BASE_API,
//   timeout: 10000, //超时时间,10秒
// })
axios.defaults.baseURL = import.meta.env.VITE_APP_BASE_API
axios.defaults.timeout = 10000 //超时时间,10秒

//request 实例添加请求与响应拦截器
axios.interceptors.request.use(
  (config) => {
    // 配置请求头
    config.headers.token = '80c483d59ca86ad0393cf8a98416e2a1' // 这里自定义配置，这里传的是token
    // config.headers['Content-Type'] = 'application/json;charset=UTF-8' // 传参方式json
    config.headers['Content-Type'] = 'application/x-www-form-urlencoded' // 传参方式表单

    return config
  },
  (error) => {
    return Promise.reject(error)
  }
)

axios.interceptors.response.use(
  (response) => {
    //成功回调
    return response
  },
  (error) => {
    const { response } = error
    if (response) {
      // 请求已发出，但是不在2xx的范围
      let errorMsg = showMessage(response.status) // 传入响应码，匹配响应码对应信息
      console.log(errorMsg)
      return Promise.reject(response.data)
    } else {
      console.log('网络连接异常,请稍后再试!')
    }
  }
)
const get = (url: string, params: Object) => {
  return new Promise((resolve, reject) => {
    axios
      .get(url, {
        params: params,
      })
      .then((res) => {
        resolve(res.data)
      })
      .catch((err) => {
        reject(err.data)
      })
  })
}

const post = (url: string, params: Object) => {
  return new Promise((resolve, reject) => {
    axios
      .post(url, QS.stringify(params)) //是将对象 序列化成URL的形式，以&进行拼接
      .then((res) => {
        resolve(res.data)
      })
      .catch((err) => {
        reject(err.data)
      })
  })
}

export default {
  axios,
  post,
  get,
}
